# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F


# See
#   https://arxiv.org/pdf/1505.04597.pdf
#   https://github.com/milesial/Pytorch-UNet


# Batch-normalized convolution layer
class Conv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv = nn.Conv2d(in_channels, out_channels, 3)
        self.norm = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
    
    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.relu(x)
        return x


# Double convolution layer
class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = Conv(in_channels, out_channels)
        self.conv2 = Conv(out_channels, out_channels)
    
    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        return x


# Down-leveling layer
class Down(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.pool = nn.MaxPool2d(2)
        self.conv = DoubleConv(in_channels, out_channels)
    
    def forward(self, x):
        x = self.pool(x)
        x = self.conv(x)
        return x


# Up-leveling layer
class Up(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.conv = DoubleConv(in_channels * 2, out_channels)
    
    def forward(self, x_older_larger, x_current):
        x_current = self.up(x_current)
        dh = x_older_larger.shape[2] - x_current.shape[2]
        dw = x_older_larger.shape[3] - x_current.shape[3]
        x_older_larger = x_older_larger[:, :, dh // 2 : dh // 2 - dh, dw // 2 : dw // 2 - dw]
        x = torch.cat([x_older_larger, x_current], dim=1)
        x = self.conv(x)
        return x


# U-Net architecture
class UNet(nn.Module):
    def __init__(self, in_channels=1, out_channels=2, top_channels=64, levels=4):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.top_channels = top_channels
        self.levels = levels
        self.conv_in = DoubleConv(in_channels, top_channels)
        def channels_at(level):
            level = max(min(level, levels - 1), 0)
            return top_channels * 2 ** level
        self.downs = []
        self.ups = []
        for level in range(levels):
            down = Down(channels_at(level), channels_at(level + 1))
            self.downs.append(down)
            self.add_module(f'down_{level}', down)
            up = Up(channels_at(level), channels_at(level - 1))
            self.ups.append(up)
            self.add_module(f'up_{level}', up)
        self.conv_out = nn.Conv2d(top_channels, out_channels, 1)
    
    def forward(self, x):
        x = self.conv_in(x)
        xs = []
        for level in range(self.levels):
            xs.append(x)
            x = self.downs[level](x)
        for level in reversed(range(self.levels)):
            x = self.ups[level](xs[level], x)
        x = self.conv_out(x)
        return x
    
    # Compute tensor shape after processing
    def output_shape(self, shape, all_steps=False):
        
        # For levels == 3, optimal input_sizes are 92+8*i
        
        # Input layer
        steps = []
        steps.append(shape)
        h, w = shape
        
        # Go down
        h -= 4
        w -= 4
        steps.append((h, w))
        for down in self.downs:
            h = h // 2 - 4
            w = w // 2 - 4
            steps.append((h, w))
        for up in self.ups:
            if h < 1 or w < 1:
                raise ValueError(shape)
            h = h * 2 - 4
            w = w * 2 - 4
            steps.append((h, w))
        steps.append((h, w))
        if all_steps:
            return steps
        return steps[-1]
