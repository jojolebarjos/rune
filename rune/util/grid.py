# -*- coding: utf-8 -*-


import numpy
from PIL import Image, ImageDraw


# Generate random square at given location
def random_square_centered_at(x, y, size):
    
    # Start from unit square as origin
    coordinates = numpy.array([
        (-0.5, -0.5), # Top left
        (-0.5,  0.5), # Bottom left
        ( 0.5,  0.5), # Bottom right
        ( 0.5, -0.5)  # Top right
    ], dtype=numpy.float32)
    
    # Add random rotation
    angle = numpy.random.uniform(-1, 1) * numpy.pi * 0.1
    c = numpy.cos(angle)
    s = numpy.sin(angle)
    coordinates = coordinates @ numpy.array(((c, -s), (s, c)))
    
    # Add point-wise noise
    offsets = numpy.random.normal(0.0, 0.05, coordinates.shape).clip(-0.1, 0.1)
    coordinates += offsets
    
    # Apply scale
    coordinates *= size * numpy.random.uniform(0.8, 1.25)
    
    # Move box to actual location
    coordinates[:, 0] += x
    coordinates[:, 1] += y
    
    return coordinates


# Get patch based on single square
def extract_square(image, coordinates, size, interpolation=Image.BILINEAR):
    return image.transform((size, size), Image.QUAD, coordinates.reshape(-1), interpolation)


# Show square on input image
def paint_square(image, coordinates, copy=True):
    if copy:
        image = image.convert('RGB')
    draw = ImageDraw.Draw(image)
    draw.polygon(coordinates.reshape(-1).tolist(), outline=255)
    return image


# Build grid from square
def grid_from_square(coordinates, xcount, ycount):
    result = numpy.zeros((xcount + 1, ycount + 1, 2), dtype=numpy.float32)
    for ix in range(xcount + 1):
        x = ix / xcount
        a = coordinates[0] + (coordinates[3] - coordinates[0]) * x
        b = coordinates[1] + (coordinates[2] - coordinates[1]) * x
        for iy in range(ycount + 1):
            y = iy / ycount
            result[ix, iy] = a + (b - a) * y
    return result


# Apply smooth distorition to grid
def random_grid_distortion(coordinates, magnitude=0.3):
    # TODO improve strategy
    w, h, _ = coordinates.shape
    size = numpy.sqrt(((coordinates[0, 0, 0] - coordinates[-1, 0, 0]) ** 2).sum()) / w
    field = numpy.random.normal(scale=size * magnitude, size=(w + 2, h + 2, 2)).clip(-0.5 * size, 0.5 * size)
    field[0, 1:-1] = field[1, 1:-1]
    field[-1, 1:-1] = field[-2, 1:-1]
    field[:, 0] = field[:, 1]
    field[:, -1] = field[:, -2]
    field = (field[:-2, :] + 2 * field[1:-1, :] + field[2:, :]) / 4
    field = (field[:, :-2] + 2 * field[:, 1:-1] + field[:, 2:]) / 4
    return coordinates + field


# Get patch based on grid
def extract_grid(image, coordinates, cell_size, interpolation=Image.BICUBIC):
    w, h, _ = coordinates.shape
    xcount = w - 1
    ycount = h - 1
    mesh = []
    for ix in range(xcount):
        for iy in range(ycount):
            box = (ix * cell_size, iy * cell_size, (ix + 1) * cell_size, (iy + 1) * cell_size)
            quad = numpy.asarray((
                coordinates[ix, iy],
                coordinates[ix, iy + 1],
                coordinates[ix + 1, iy + 1],
                coordinates[ix + 1, iy]
            )).reshape(-1)
            part = (box, quad)
            mesh.append(part)
    return image.transform((xcount * cell_size, ycount * cell_size), Image.MESH, mesh, interpolation)


# Show grid on input image
def paint_grid(image, coordinates, copy=True):
    if copy:
        image = image.convert('RGB')
    draw = ImageDraw.Draw(image)
    xcount, ycount, _ = coordinates.shape
    for ix in range(xcount):
        draw.line(coordinates[ix].reshape(-1).tolist(), fill=255)
    for iy in range(ycount):
        draw.line(coordinates[:, iy].reshape(-1).tolist(), fill=255)
    return image


# Choose sampling location on specified label image
def generate_centers(label, num_centers, margin):
    label_array = numpy.asarray(label)
    height, width = label_array.shape
    # TODO maybe focus on area with text?
    # TODO maybe add some coverage guarantee (i.e. sample full grid)?
    centers = []
    while len(centers) < num_centers:
        # TODO maybe add retry limit
        x = numpy.random.uniform(margin, width - margin)
        y = numpy.random.uniform(margin, height - margin)
        ix = int(x)
        iy = int(y)
        if label_array[int(y), int(x)] != 0:
            center = (x, y)
            centers.append(center)
    return centers
