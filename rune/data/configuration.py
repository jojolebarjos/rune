# -*- coding: utf-8 -*-


class Configuration:
    def __init__(self):
        
        # General parameters
        self.device = 'cpu'
        self.batch_size = 32
        
        # Image normalization
        self.standard_thickness = 10
        
        # Detector parameters
        # TODO use detector_ prefix
        self.window_size = 128 # Square window size in pixels, in standard format
        self.sample_size = 64  # Square sample size in pixels
        self.overlap = 0.5
        self.grid_resolution = 8
        
        # Detector label parameters
        self.margin_ratio_above = 1.0
        self.margin_ratio_below = 1.0
        self.margin_ratio_begin = 2.0
        self.margin_ratio_end = 2.0
        
        # Detector cache parameters
        # TODO cache image format
        # TODO whether to actually cache color image
