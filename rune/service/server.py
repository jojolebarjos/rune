# -*- coding: utf-8 -*-


import os
import random

from fastapi import FastAPI, HTTPException
from starlette.responses import FileResponse, JSONResponse, RedirectResponse

from rune.data import Workspace, Annotation


HERE = os.path.dirname(os.path.realpath(__file__))
LINE_HTML = os.path.join(HERE, 'interface.html')
WORKSPACE_FOLDER = 'workspace'


workspace = Workspace('workspace')


app = FastAPI(
    # TODO get properties from module (same source as setup.py)
    title = 'Rune Annotation Service',
    description = 'Automated Handwritten Character Recognition',
    version = '0.0.1'
)


# TODO clean API
# TODO add endpoints documentation


@app.exception_handler(KeyError)
def key_error_handler(request, e):
    return JSONResponse(status_code=404, content={
        'message': f'invalid key {e}'
    })


@app.get('/api/collection/')
def get_workspace():
    return {'collections': workspace.keys()}


@app.get('/api/collection/{collection_key}/')
def get_collection(collection_key: str):
    return {'samples': workspace[collection_key].keys()}


@app.get('/api/collection/{collection_key}/suggest')
def get_collection_suggestion(collection_key: str):
    keys = workspace[collection_key].keys()
    return {
        'key': random.choice(keys)
    }


@app.get('/api/collection/{collection_key}/sample/{item_key}/')
def get_item(collection_key: str, item_key: str):
    return {'annotations': workspace[collection_key][item_key].keys()}


@app.get('/api/collection/{collection_key}/sample/{item_key}/image.jpg')
def get_item_image(collection_key: str, item_key: str):
    print('wow')
    item = workspace[collection_key][item_key]
    return FileResponse(item.image_path)


@app.get('/api/collection/{collection_key}/sample/{item_key}/annotation/{annotation_key}', response_model=Annotation)
def get_item_annotation(collection_key: str, item_key: str, annotation_key: str):
    return workspace[collection_key][item_key][annotation_key]


@app.put('/api/collection/{collection_key}/sample/{item_key}/annotation/{annotation_key}')
def put_item_annotation(collection_key: str, item_key: str, annotation_key: str, value: Annotation):
    workspace[collection_key][item_key][annotation_key] = value


@app.get('/line.html')
def get_line_html():
    return FileResponse(LINE_HTML)


@app.get('/')
def get_root():
    return RedirectResponse(url='/line.html')


# TODO can we use *.js?
# TODO maybe do not keep external lib in this repo?
@app.get('/js/d3.v5.min.js')
def get_line_html():
    return FileResponse(os.path.join(HERE, 'd3.v5.min.js'))
