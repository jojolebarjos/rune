# -*- coding: utf-8 -*-


import numpy

import torch
import torch.nn as nn
import torch.nn.functional as F

from .unet import UNet


# Training helper
class DetectorTrainer:
    def __init__(self, configuration, sampler):
        
        # Keep configuration
        self.configuration = configuration
        self.sampler = sampler
        self.sampler_iterator = iter(sampler)
        
        # Create model
        # TODO get these parameters from somewhere
        # TODO finer parameters
        self.model = UNet(
            in_channels = 1,
            out_channels = 6,
            top_channels = 4,
            levels = 2
        ).to(configuration.device)
        self.model.train()
        
        # Compute output size
        self.input_shape = (configuration.sample_size, configuration.sample_size)
        self.output_shape = self.model.output_shape(self.input_shape)
        
        # Define criterion
        # TODO class weights from configuration?
        self.class_weight = torch.tensor([1, 1, 0.5, 0.5, 2, 2])
        self.criterion = nn.CrossEntropyLoss(
            weight = self.class_weight.to(configuration.device),
            reduction = 'none'
        )
        
        # Define optimizer
        # TODO optimizer and learning rate from configuration
        self.optimizer = torch.optim.Adam(
            params = self.model.parameters(),
            lr = 0.005
        )
    
    # Generate next batch
    def next_batch(self):
        
        # Acquire samples
        images = []
        labels = []
        for i in range(self.configuration.batch_size):
            pair = next(self.sampler_iterator, None)
            if pair is None:
                break
            image, label = pair
            images.append(image)
            labels.append(label)
        
        # Handle end-of-stream
        if len(images) == 0:
            raise StopIteration()
        
        # Pack as tensors
        images = numpy.stack(images)
        labels = numpy.stack(labels)
        return images, labels
    
    # Prepare image tensor
    def prepare_images(self, images):
        
        # Normalize content
        images = images.astype(numpy.float32)
        images *= 2.0 / 255.0
        images -= 1.0
        
        # TODO maybe add noise
        
        # Convert to tensor
        images = images[:, None, :, :]
        images = torch.from_numpy(images)
        return images
    
    # Prepare label tensor
    def prepare_labels(self, labels):
        
        # Crop to match model output
        ih, iw = self.input_shape
        oh, ow = self.output_shape
        dh = ih - oh
        dw = iw - ow
        labels = labels[:, dh // 2 : dh // 2 - dh, dw // 2 : dw // 2 - dw]
        
        # Extract weights
        weights = (labels > 0).astype(numpy.float32)
        labels[labels == 0] = 1
        labels -= 1
        labels = labels.astype(numpy.int64)
        
        # Convert to tensor
        labels = torch.from_numpy(labels)
        weights = torch.from_numpy(weights)
        return labels, weights
    
    # Execute a single training step
    def do_step(self):
        
        # Get batch as tensor
        images, labels = self.next_batch()
        images = self.prepare_images(images)
        labels, weights = self.prepare_labels(labels)
        images = images.to(self.configuration.device)
        weights = weights.to(self.configuration.device)
        labels = labels.to(self.configuration.device)
        
        # Apply model
        logits = self.model(images)
        
        # Flatten
        # TODO use permute instead
        flat_logits = logits.transpose(1, 3).transpose(1, 2).contiguous().view(-1, logits.shape[1])
        flat_labels = labels.view(-1)
        flat_weights = weights.view(-1)
        
        # Compute weighted loss
        loss = self.criterion(flat_logits, flat_labels)
        loss = (loss * flat_weights).sum() / flat_weights.sum()
        
        # Backpropagate
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        
        # Report loss
        loss = float(loss.detach().cpu())
        return loss
    
    # TODO save/load methods
    
    # TODO helper to apply on test image
