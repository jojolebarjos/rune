# -*- coding: utf-8 -*-


import numpy
import random


# TODO use actual language model for that... Or at least, pairwise probabilities...


ENGLISH_FREQUENCIES = {
    'a': 8.167,
    'b': 1.492,
    'c': 2.782,
    'd': 4.253,
    'e': 12.702,
    'f': 2.228,
    'g': 2.015,
    'h': 6.094,
    'i': 6.966,
    'j': 0.153,
    'k': 0.772,
    'l': 4.025,
    'm': 2.406,
    'n': 6.749,
    'o': 7.507,
    'p': 1.929,
    'q': 0.095,
    'r': 5.987,
    's': 6.327,
    't': 9.056,
    'u': 2.758,
    'v': 0.978,
    'w': 2.360,
    'x': 0.150,
    'y': 1.974,
    'z': 0.074
}

FRENCH_FREQUENCIES = {
    'a': 7.11,
    'à': 0.31,
    'â': 0.03,
    'b': 1.14,
    'c': 3.18,
    'ç': 0.06,
    'd': 3.67,
    'e': 12.10,
    'é': 1.94,
    'è': 0.31,
    'ê': 0.08,
    'ë': 0.01,
    'f': 1.11,
    'g': 1.23,
    'h': 1.11,
    'i': 6.59,
    'î': 0.03,
    'ï': 0.01,
    'í': 0.01,
    'j': 0.34,
    'k': 0.29,
    'l': 4.96,
    'm': 2.62,
    'n': 6.39,
    'o': 5.02,
    'ô': 0.04,
    'ö': 0.01,
    'p': 2.49,
    'q': 0.65,
    'r': 6.07,
    's': 6.51,
    't': 5.92,
    'u': 4.49,
    'û': 0.02,
    'ù': 0.02,
    'ü': 0.01,
    'v': 1.11,
    'w': 0.17,
    'x': 0.38,
    'y': 0.46,
    'z': 0.15
}


class TextGenerator:
    def __init__(self):
        frequencies = ENGLISH_FREQUENCIES
        # frequencies = {c : f + 50 for c, f in frequencies.items()}
        self.letters, self.letter_probabilities = zip(*frequencies.items())
    
    def random_word(self, start_of_sentence=False):
        length = int(numpy.random.normal(3.5, 3.5))
        length = max(min(length, 8), 1)
        word = ''.join(random.choices(self.letters, self.letter_probabilities, k=length))
        upper_probability = 0.8 if start_of_sentence else 0.1
        if random.random() < upper_probability:
            word = word[0].upper() + word[1:]
        return word
    
    def random_text(self, length):
        text = ''
        while len(text) < length:
            start_of_sentence = len(text) == 0
            text += self.random_word(start_of_sentence)
            text += ' '
        return text.rstrip()
