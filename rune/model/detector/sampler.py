# -*- coding: utf-8 -*-


import io
import numpy
import pickle
from PIL import Image

from rune.util import random_square_centered_at, grid_from_square, random_grid_distortion, extract_grid


# Sample patches from a single image
class SingleSampler:
    def __init__(self, image, label, configuration):
        
        # Keep data
        self.image = image
        self.label = label
        self.configuration = configuration
        self.sample_cell_size = configuration.sample_size // configuration.grid_resolution
        
        # Compute spacing
        width, height = image.size
        x_count = int((width - configuration.window_size) // (configuration.window_size * (1 - configuration.overlap)) + 1)
        y_count = int((height - configuration.window_size) // (configuration.window_size * (1 - configuration.overlap)) + 1)
        x_delta = (width - configuration.window_size) / (x_count - 1)
        y_delta = (height - configuration.window_size) / (y_count - 1)
        
        # Generate sampling grid
        label_array = numpy.asarray(label)
        self.centers = []
        for ix in range(x_count):
            for iy in range(y_count):
                x = ix * x_delta + configuration.window_size / 2
                y = iy * y_delta + configuration.window_size / 2
                if label_array[int(y), int(x)] != 0:
                    center = (x, y)
                    self.centers.append(center)
        self.centers = numpy.asarray(self.centers)
        
    # Get number of patches per pass
    def __len__(self):
        return len(self.centers)
    
    # Iterate over randomized patches
    def __iter__(self):
        
        # Iterate over predefined centers
        indices = numpy.arange(len(self.centers))
        numpy.random.shuffle(indices)
        for i in indices:
            
            # Get center with some noise
            x, y = self.centers[i]
            x += numpy.clip(numpy.random.normal(), -3, 3) * self.configuration.window_size / 6
            y += numpy.clip(numpy.random.normal(), -3, 3) * self.configuration.window_size / 6
            
            # Generate random grid
            square = random_square_centered_at(x, y, self.configuration.window_size)
            grid = grid_from_square(square, self.configuration.grid_resolution, self.configuration.grid_resolution)
            grid = random_grid_distortion(grid)
            
            # Extract raw patches
            patch_image = extract_grid(self.image, grid, self.sample_cell_size)
            patch_label = extract_grid(self.label, grid, self.sample_cell_size, interpolation=Image.NEAREST)
            
            # Convert to numpy
            patch_image = numpy.asarray(patch_image)
            patch_label = numpy.asarray(patch_label)
            yield patch_image, patch_label


# Sample patches from multiple images
class MultiSampler:
    def __init__(self, samplers):
        self.samplers = samplers
        self.indices = []
        for i, sampler in enumerate(samplers):
            self.indices.extend([i] * len(sampler))
        self.indices = numpy.asarray(self.indices)
    
    # Get number of patches per pass
    def __len__(self):
        return len(self.indices)
    
    # Iterate over randomized patches
    def __iter__(self):
        indices = self.indices.copy()
        numpy.random.shuffle(indices)
        iterators = [iter(sampler) for sampler in self.samplers]
        for i in indices:
            yield next(iterators[i])
    
    # Create from image cache
    @classmethod
    def from_cache(cls, cache):
        samplers = []
        for image, label in cache:
            sampler = SingleSampler(image, label, cache.configuration)
            samplers.append(sampler)
        return cls(samplers)


# Sample patches from cache
class CachedSampler:
    def __init__(self, path):
        self.path = path
        with io.open(path, 'rb') as file:
            self.data = pickle.load(file)
    
    # Get number of patches per pass
    def __len__(self):
        return len(self.data)
    
    # Iterate over randomized patches
    def __iter__(self):
        indices = numpy.arange(len(self.data))
        numpy.random.shuffle(indices)
        for i in indices:
            yield self.data[i]
    
    # Create cache from actual sampler
    @staticmethod
    def create(path, sampler):
        data = list(iter(sampler))
        data = numpy.asarray(data)
        with io.open(path, 'wb') as file:
            pickle.dump(data, file)
