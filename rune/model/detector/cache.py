# -*- coding: utf-8 -*-


import numpy
import os
from PIL import Image, ImageDraw
import scipy.ndimage.filters
import skimage.morphology

from rune.data import LineHelper, AreaHelper
from rune.util import as_array, as_image, normalize, resize


# Detector labels
LABEL_COUNT = 7
(
    LABEL_IGNORED,
    LABEL_OUTSIDE,
    LABEL_INSIDE,
    LABEL_ABOVE,
    LABEL_BELOW,
    LABEL_BEGIN,
    LABEL_END
) = range(LABEL_COUNT)

# Scale grayscale for debug
LABEL_BIAS = 32


# Cache for preprocessed samples
class DetectorCache:
    def __init__(self, folder, configuration):
        
        # Store data in a single folder
        self.folder = folder
        if os.path.exists(folder):
            assert os.path.isdir(folder)
        else:
            os.makedirs(folder)
        
        # Keep configuration
        self.configuration = configuration
        
        # Inspect folder
        self.names = []
        self.next_id = 0
        self.refresh()
    
    # Reload available files from disk
    def refresh(self):
        self.names = []
        largest_id = -1
        for name in os.listdir(self.folder):
            if name.endswith('.label.png'):
                name = name[:-10]
                self.names.append(name)
                if name.isdigit():
                    id = int(name.lstrip('0') or '0')
                    largest_id = max(largest_id, id)
        self.next_id = largest_id + 1
    
    # Get total number of samples
    def __len__(self):
        return len(self.names)
    
    # Get specific sample
    def __getitem__(self, index):
        
        # Get base name
        name = self.names[index]
        path = os.path.join(self.folder, name)
        
        # Load image
        image_path = path + '.filtered.png'
        image = Image.open(image_path).convert('L')
        image.load()
        
        # Load label
        label_path = path + '.label.png'
        label = Image.open(label_path).convert('L')
        label.load()
        label = numpy.asarray(label) // LABEL_BIAS
        label = Image.fromarray(label)
        
        return image, label
    
    # Store new sample in cache
    def add(self, image_path, annotation):
        
        # Increment id
        id = self.next_id
        self.next_id += 1
        
        # Load original image
        original_image = Image.open(image_path)
        original_image.load()
        
        # Estimate thickness, if needed
        expected_thickness = annotation.thickness
        if expected_thickness is None:
            expected_thickness = numpy.median([line.thickness for line in annotation.lines])
        
        # Resize image
        ratio = self.configuration.standard_thickness / expected_thickness
        base_image = resize(original_image, ratio)
        
        # Export base image
        base_image_path = os.path.join(self.folder, f'{id:06d}.base.png')
        base_image.save(base_image_path)
        
        # Filter image
        filtered_image = base_image.convert('L')
        filtered_image = as_array(filtered_image)
        filtered_image = normalize(filtered_image)
        filtered_image = as_image(filtered_image)
        
        # Export filtered image
        filtered_image_path = os.path.join(self.folder, f'{id:06d}.filtered.png')
        filtered_image.save(filtered_image_path)
        
        # Build label image
        label_image = self._create_label_field(filtered_image.size, annotation, ratio)
        
        # Export label image
        label_image_path = os.path.join(self.folder, f'{id:06d}.label.png')
        label_image.save(label_image_path)
        
        return id
        
    # Generate label image
    def _create_label_field(self, size, annotation, ratio):
        
        # Create buffer image
        image = Image.new('L', size, color=LABEL_OUTSIDE*LABEL_BIAS)
        draw = ImageDraw.Draw(image)
        
        # Generate line helpers
        lines = [LineHelper(line, ratio) for line in annotation.lines]
        
        # Add end
        for line in lines:
            polygon = numpy.stack([
                line.points[-1] + line.boundaries[-1] * (-self.configuration.margin_ratio_below),
                line.points[-1] + line.boundaries[-1] * (-self.configuration.margin_ratio_below) + line.forwards[-1] * line.thickness * self.configuration.margin_ratio_end,
                line.points[-1] + line.boundaries[-1] * (1 + self.configuration.margin_ratio_above) + line.forwards[-1] * line.thickness * self.configuration.margin_ratio_end,
                line.points[-1] + line.boundaries[-1] * (1 + self.configuration.margin_ratio_above)
            ], axis=0)
            draw.polygon(polygon, fill=LABEL_END*LABEL_BIAS)
        
        # Add begin
        for line in lines:
            polygon = numpy.stack([
                line.points[0] + line.boundaries[0] * (-self.configuration.margin_ratio_below) - line.forwards[0] * line.thickness * self.configuration.margin_ratio_begin,
                line.points[0] + line.boundaries[0] * (-self.configuration.margin_ratio_below),
                line.points[0] + line.boundaries[0] * (1 + self.configuration.margin_ratio_above),
                line.points[0] + line.boundaries[0] * (1 + self.configuration.margin_ratio_above) - line.forwards[0] * line.thickness * self.configuration.margin_ratio_begin
            ], axis=0)
            draw.polygon(polygon, fill=LABEL_BEGIN*LABEL_BIAS)
        
        # Add above
        for line in lines:
            polygon = numpy.concatenate([
                (line.points + line.boundaries * 0.5),
                (line.points + line.boundaries * (1 + self.configuration.margin_ratio_above))[::-1]
            ], axis=0)
            draw.polygon(polygon, fill=LABEL_ABOVE*LABEL_BIAS)
        
        # Add below
        for line in lines:
            polygon = numpy.concatenate([
                (line.points + line.boundaries * (-self.configuration.margin_ratio_below)),
                (line.points + line.boundaries * 0.5)[::-1]
            ], axis=0)
            draw.polygon(polygon, fill=LABEL_BELOW*LABEL_BIAS)
        
        # Add inside
        for line in lines:
            polygon = numpy.concatenate([
                (line.points),
                (line.points + line.boundaries)[::-1]
            ], axis=0)
            draw.polygon(polygon, fill=LABEL_INSIDE*LABEL_BIAS)
        
        # Add ignored areas
        if annotation.areas:
            for area in annotation.areas:
                if area.tags and 'exclude' in area.tags:
                    area = AreaHelper(area, ratio)
                    draw.polygon(area.points, fill=LABEL_IGNORED*LABEL_BIAS)
        
        # Ready
        return image
