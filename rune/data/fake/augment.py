# -*- coding: utf-8 -*-


import numpy
from PIL import Image

from rune.data.helper import LineHelper


class PatchAugmentor:
    
    def resample(self, patch, baseline, thickness, height=None, extent_ratio=4.0):
        
        # Load as image
        patch_height, patch_width = patch.shape
        patch = Image.fromarray(patch)
        
        # Create helper
        points = [(0, baseline), (patch_width, baseline)]
        line = LineHelper.line_from_array(points, thickness)
        line = LineHelper(line)
        
        # Randomize
        line = line.resample(margin=thickness)
        line = LineHelper(line)
        line = line.randomize()
        line = LineHelper(line)
        
        # Extract patch
        patch = line.extract(patch, height=height, extent_ratio=extent_ratio)
        return patch
    
    # TODO add noise
    
    def pad(self, patch, width):
        
        # Convert to array
        patch = numpy.asarray(patch)
        h, w = patch.shape
        
        # Estimate left margin
        left = 0
        while left < w and patch[:, left].sum() == 0:
            left += 1
        
        # Handle empty image
        if left >= w:
            patch = numpy.zeros((h, width), dtype=numpy.uint8)
        
        # Otherwise, generate padding
        else:
            
            # Estimate right margin
            right = w - 1
            while right > left and patch[:, right].sum() == 0:
                right -= 1
            
            # Estimate required padding
            actual_w = right - left + 1
            padding = width - actual_w
            assert padding >= 0, 'image is too wide'
            
            # Add random padding
            if padding > 0:
                left_padding = numpy.random.randint(padding + 1)
                right_padding = padding - left_padding
                patch = numpy.concatenate([
                    numpy.zeros((h, left_padding), dtype=numpy.uint8),
                    patch[:, left:right+1],
                    numpy.zeros((h, right_padding), dtype=numpy.uint8)
                ], axis=1)
        
        # Convert back to image
        patch = Image.fromarray(patch)
        return patch
