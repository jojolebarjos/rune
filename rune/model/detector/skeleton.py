# -*- coding: utf-8 -*-


import numpy


# 8-connected pixel neighbors offsets
DELTAS = [
    ( 0,  1),
    (-1,  1),
    (-1,  0),
    (-1, -1),
    ( 0, -1),
    ( 1, -1),
    ( 1,  0),
    ( 1,  1)
]


# Approximate binary skeleton
def fit_graph(skeleton):
    
    # Compute skeleton pixel neighbor count
    count = numpy.zeros(skeleton.shape, dtype=numpy.int32)
    count[1:-1, 1:-1] += skeleton[1:-1, 2:  ] # E
    count[1:-1, 1:-1] += skeleton[ :-2, 2:  ] # NE
    count[1:-1, 1:-1] += skeleton[ :-2, 1:-1] # N
    count[1:-1, 1:-1] += skeleton[ :-2,  :-2] # NW
    count[1:-1, 1:-1] += skeleton[1:-1,  :-2] # W
    count[1:-1, 1:-1] += skeleton[2:  ,  :-2] # SW
    count[1:-1, 1:-1] += skeleton[2:  , 1:-1] # S
    count[1:-1, 1:-1] += skeleton[2:  , 2:  ] # SE
    
    # Find points of interest on skeleton
    ends = count == 1
    crosses = count > 2
    nodes = numpy.logical_and(numpy.logical_or(ends, crosses), skeleton)
    
    # Cluster nodes
    node_points, clusters, centroids = find_clusters(nodes)
    assignment = numpy.zeros(len(node_points), dtype=numpy.int32)
    for i, cluster in enumerate(clusters):
        for j in cluster:
            assignment[j] = i
    
    # Collect anchor points, associated to its cluster
    anchors = {}
    islands = {}
    for i, point in enumerate(node_points):
        y, x = point
        for dy, dx in DELTAS:
            ny = y + dy
            nx = x + dx
            if skeleton[ny, nx] and not nodes[ny, nx]:
                anchor = (ny, nx)
                if anchor in anchors:
                    assert anchor not in islands
                    islands[anchor] = assignment[i]
                else:
                    anchors[anchor] = assignment[i]
    
    # Follow each anchor to build polylines
    vertices = list(centroids)
    edges = []
    remaining = set(anchors.keys())
    while len(remaining) > 0:
        start_anchor = remaining.pop()
        
        # Handle length 3 segments
        if start_anchor in islands:
            edge = (anchors[start_anchor], islands[start_anchor])
            edges.append(edge)
        
        # Otherwise, follow points until another anchor is reached
        else:
            
            # Use centroid coordinate as starting point
            points = [
                centroids[anchors[start_anchor]],
                start_anchor
            ]
            
            # Follow actual pixels
            last_point = None
            point = start_anchor
            while True:
                y, x = point
                
                # Check for endpoint, i.e. whether an anchor has been reached
                if point in remaining:
                    end_anchor = point
                    remaining.remove(end_anchor)
                    points.append(centroids[anchors[end_anchor]])
                    break
                
                # Look around for next pixel
                for dy, dx in DELTAS:
                    ny = y + dy
                    nx = x + dx
                    next_point = (ny, nx)
                    if next_point != last_point and skeleton[ny, nx] and not nodes[ny, nx]:
                        break
                else:
                    assert False
                
                # Move pointer
                last_point = point
                point = next_point
                points.append(point)
            
            # Build polyline
            indices = select_polyline_points(points)
            
            # Use nodes as start/end vertices
            sequence = numpy.zeros(len(indices), dtype=numpy.int32)
            sequence[0] = anchors[start_anchor]
            sequence[-1] = anchors[end_anchor]
            
            # Add inner points to vertices
            for i in range(1, len(indices) - 1):
                sequence[i] = len(vertices)
                vertices.append(points[indices[i]])
            
            # Add edges
            for i in range(len(indices) - 1):
                edges.append((sequence[i], sequence[i + 1]))
    
    # Convert to arrays
    vertices = numpy.array(vertices, dtype=numpy.float32)
    edges = numpy.array(edges, dtype=numpy.int32)
    return vertices, edges


# Find connected components and compute centroids
def find_clusters(binary):
    
    # Get points
    points = list(zip(*numpy.nonzero(binary)))
    
    # Build adjacency list
    # TODO use deltas instead, as it will be O(8n)
    neighbors = [[] for _ in points]
    for i in range(len(points)):
        iy, ix = points[i]
        for j in range(i + 1, len(points)):
            jy, jx = points[j]
            if numpy.abs(jy - iy) <= 1 and numpy.abs(jx - ix) <= 1:
                neighbors[i].append(j)
                neighbors[j].append(i)
    
    # Find connected components
    remaining = set(range(len(points)))
    clusters = []
    while len(remaining) > 0:
        i = remaining.pop()
        cluster = {i}
        def recurse(i):
            for j in neighbors[i]:
                if j not in cluster:
                    cluster.add(j)
                    remaining.remove(j)
                    recurse(j)
        recurse(i)
        clusters.append(cluster)
    
    # Compute centroids
    centroids = []
    for cluster in clusters:
        y = 0
        x = 0
        for i in cluster:
            iy, ix = points[i]
            y += iy
            x += ix
        y /= len(cluster)
        x /= len(cluster)
        centroids.append((y, x))
    
    # Return everything
    return points, clusters, centroids


# Fit polyline to pixel sequence
def select_polyline_points(points, min_count=5, distance_threshold=2):
    
    # Make sure we have a float array
    points = numpy.asarray(points, dtype=numpy.float32)
    
    # Recursively split segments, until compliant
    def recurse(a, b):
        
        # Small segments are not split (using point count for simplicity)
        if b - a <= min_count:
            return []
        
        # Compute point distances to segment
        direction = points[b] - points[a]
        normal = numpy.array((-direction[1], direction[0]))
        normal /= numpy.sqrt((normal * normal).sum())
        delta = points[a:b] - points[a, :]
        distance = numpy.abs((delta * normal[numpy.newaxis, :]).sum(axis=1))
        
        # Find most distant point and check for tolerance
        index = distance.argmax()
        if distance[index] <= distance_threshold:
            return []
        
        # Split at given location
        m = index + a
        return [*recurse(a, m), m, *recurse(m, b)]
    
    # Start by considering the whole sequence
    a = 0
    b = len(points) - 1
    indices = [a, *recurse(a, b), b]
    return indices
