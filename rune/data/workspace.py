# -*- coding: utf-8 -*-


import io
import json
import os

from typing import Optional

from .annotation import Annotation


# TODO validate key characters


class _DictLike:
    
    def __len__(self):
        return len(self.keys())
    
    def __iter__(self):
        return self.keys()
    
    def __contains__(self, key):
        return key in self.keys()
    
    def items(self):
        for key in self.keys():
            yield key, self[key]
    
    def values(self):
        for key in self.keys():
            yield self[key]


class Sample(_DictLike):
    def __init__(self, image_path):
        self.image_path = image_path
    
    def keys(self):
        folder = os.path.dirname(self.image_path)
        prefix = os.path.basename(self.image_path)[:-3]
        keys = []
        for name in os.listdir(folder):
            if name.startswith(prefix) and name.endswith('.json'):
                key = name[len(prefix):-5]
                keys.append(key)
        return keys
    
    def __getitem__(self, key: str) -> Optional[Annotation]:
        annotation_path = f'{self.image_path[:-3]}{key}.json'
        if not os.path.exists(annotation_path):
            raise KeyError(key)
        with io.open(annotation_path, 'r', encoding='utf-8') as file:
            value = json.load(file)
        value = Annotation.parse_obj(value)
        return value
    
    def __setitem__(self, key: str, value: Annotation):
        annotation_path = f'{self.image_path[:-3]}{key}.json'
        value = value.dict(skip_defaults=True)
        with io.open(annotation_path, 'w', encoding='utf-8') as file:
            json.dump(value, file, indent=2)


class Collection(_DictLike):
    def __init__(self, folder: str):
        self.folder = folder
    
    def keys(self):
        keys = []
        for name in os.listdir(self.folder):
            if name.endswith('.jpg'):
                keys.append(name[:-4])
        return keys
    
    def __getitem__(self, key: str) -> Sample:
        image_path = os.path.join(self.folder, key + '.jpg')
        if not os.path.isfile(image_path):
            raise KeyError(key)
        return Sample(image_path)


class Workspace(_DictLike):
    def __init__(self, folder: str):
        self.folder = folder
    
    def keys(self):
        keys = []
        for name in os.listdir(self.folder):
            subfolder = os.path.join(self.folder, name)
            if os.path.isdir(subfolder):
                keys.append(name)
        return keys
    
    def __getitem__(self, key: str) -> Collection:
        subfolder = os.path.join(self.folder, key)
        if not os.path.isdir(subfolder):
            raise KeyError(key)
        return Collection(subfolder)
