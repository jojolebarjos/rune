# -*- coding: utf-8 -*-


from pydantic import BaseModel
from typing import List, Optional, Union


class Point(BaseModel):
    """Represents a single 2D point.
    
    """
    
    x: float
    y: float


class Area(BaseModel):
    """Represents a polygon, used to mark inclusion and exclusion.
    
    In practice, some pictures have tricky parts where annotators do not want
    to spend time. It might also be partial text that is not complete enough
    to act as proper training set.
    
    Tags can be specified for specific behaviours.
    
    """
    
    points: List[Point]
    tags: Optional[List[str]]


class Span(BaseModel):
    """Represents a single chunk of text, inside a line.
    
    If annotation is partial, either as an effort to focus on some keywords, or
    to handle some unreadable text, an annotator can transcribe only some
    fragments.
    
    Start and end locations are define as ratio of the actual polyline.
    
    Tags can be specified for specific behaviours.
    
    """
    
    text: str
    start: float
    end: float
    tags: Optional[List[str]]


class Line(BaseModel):
    """Represents a contiguous sequence of characters.
    
    As orientation might change due to writing imprecisions or acquisition
    distortions, a polyline is used to follow the base line.
    
    Thickness represents the average height of letter bodies, i.e. it does not
    include upper parts of capital letters, dots on top of "i", etc. As such,
    it should not be confused with font ascent, which mark the upper limit of
    characters.
    
    This definition is made to put focus on letter core shape, as ascending and
    descending areas are less informative and cover extensive area in some
    writing styles.
    
    Polyline points should lie on the base line, i.e. as if underlining the
    text.
    
    Tags can be specified for specific behaviours.
    
    """
    
    text: Optional[Union[str, List[Span]]]
    points: List[Point]
    thickness: float
    tags: Optional[List[str]]


class Annotation(BaseModel):
    """Represents a complete annotation package for a single sample.
    
    Tags can be specified for specific behaviours.
    
    """
    
    thickness: Optional[float]
    lines: List[Line]
    areas: Optional[List[Area]]
    tags: Optional[List[str]]
