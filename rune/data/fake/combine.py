# -*- coding: utf-8 -*-


import numpy
from PIL import Image
import random

from .font import enumerate_fonts, PatchGenerator
from .augment import PatchAugmentor
from .text import TextGenerator


class FakeSampler:
    def __init__(self, font_folder='.'):
        
        # Parameters
        self.height = 48
        self.width = 384
        self.overscale = 2
        
        # Load font samplers
        self.fonts = []
        for path in enumerate_fonts(font_folder):
            font = PatchGenerator(path)
            font.set_size(48)
            self.fonts.append(font)
        
        # Load image augmentator
        self.augmentor = PatchAugmentor()
        
        # Load text generator
        self.generator = TextGenerator()
    
    def sample(self):
        
        # Choose random font
        font = random.choice(self.fonts)
        
        # Generate random text content
        text_length = numpy.random.randint(16, 24)
        text = self.generator.random_text(text_length)
        
        # Generate raw patch
        patch, baseline = font.compute_patch(text)
        thickness = font.thickness
        
        # Resample with randomization
        _, patch_width = patch.shape
        print(patch.shape)
        unpadded_width = self.width * self.overscale * (numpy.random.random() * 0.1 + 0.85)
        patch_height = patch_width * self.height * self.overscale / unpadded_width
        padded_extent_ratio = patch_height / thickness
        patch = self.augmentor.resample(patch, baseline, thickness, height=self.height * self.overscale, extent_ratio=padded_extent_ratio)
        
        # Pad horizontally
        patch = self.augmentor.pad(patch, self.width * self.overscale)
        
        # Shrink to final scale
        patch = patch.resize((self.width, self.height), Image.LANCZOS)
        
        # Ready
        return patch, text
