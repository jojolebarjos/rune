# -*- coding: utf-8 -*-


import numpy
from PIL import Image, ImageDraw
import skimage.morphology

import torch
import torch.nn as nn
import torch.nn.functional as F

from .unet import UNet
from .skeleton import fit_graph
from .graph import prune_graph, build_neighbors, build_components, build_lines

from rune.data import Line, LineHelper, Annotation
from rune.util import as_array, as_image, normalize, resize


# Tagger helper
class DetectorTagger:
    def __init__(self, configuration, model):
        
        # Keep configuration
        self.configuration = configuration
        
        if type(model) is str:
        
            # Create model
            self.model = UNet(
                in_channels = 1,
                out_channels = 6,
                top_channels = 4,
                levels = 2
            ).to(configuration.device)
            self.model.eval()
            
            # Load parameters from checkpoint
            self.model.load_state_dict(torch.load(model))
        
        else:
            self.model = model
        
        # Label color map, for debug purpose
        self.class_colors = numpy.array([
            [0, 0, 0],
            [1, 1, 1],
            [0, 0, 1],
            [1, 0, 0],
            [0, 1, 0],
            [0, 1, 0]
        ])
    
    # Normalize and resize image
    def prepare_image(self, image, expected_thickness):
        
        # Resize image to standard scale
        standard_ratio = self.configuration.standard_thickness / expected_thickness
        standard_image = resize(image, standard_ratio)
        
        # Filter image
        filtered_image = standard_image.convert('L')
        filtered_image = as_array(filtered_image)
        filtered_image = normalize(filtered_image)
        filtered_image = as_image(filtered_image)
        
        # Resize image to network scale
        network_ratio = self.configuration.sample_size / self.configuration.window_size
        network_image = resize(filtered_image, network_ratio)
        return network_image
    
    # Apply raw model
    def compute_probability(self, image):
        
        # Convert to [-1, 1] array
        array = numpy.asarray(image, dtype=numpy.float32)
        array *= 2.0 / 255.0
        array -= 1.0
        
        # Load as singleton batch
        input = torch.from_numpy(array)
        input = input.unsqueeze(0).unsqueeze(0)
        input = input.to(self.configuration.device)
        
        # Apply model
        logit = self.model(input)
        
        # Extract probability array
        probability = F.softmax(logit, dim=1)
        probability = probability.squeeze(0)
        probability = probability.permute(1, 2, 0)
        
        # Convert to array
        probability = probability.detach().cpu()
        probability = probability.numpy()
        
        # Restore padding
        dh = array.shape[0] - probability.shape[0]
        dw = array.shape[1] - probability.shape[1]
        hh = dh // 2
        hw = dw // 2
        probability = numpy.pad(probability, ((hh, dh - hh), (hw, dw - hw), (0, 0)), 'constant')
        return probability
    
    # Convert probability to debug image
    def color_probability(self, probability):
        # TODO maybe option to get alpha channel?
        debug_image = probability @ self.class_colors
        debug_image = (debug_image * 255).astype(numpy.uint8)
        debug_image = Image.fromarray(debug_image)
        return debug_image
    
    # Compute graph from probabilities
    def extract_graph(self, probability):
        
        # Consider only the "inside" class
        field = probability[:, :, 1]
        
        # Binarize
        # TODO threshold from config
        threshold = 0.25
        binary_field = field > threshold
        
        # Skeletonize
        skeleton = skimage.morphology.skeletonize(binary_field)
        
        # Hack: need to make sure nothing is on the border
        skeleton[0, :] = False
        skeleton[-1, :] = False
        skeleton[:, 0] = False
        skeleton[:, -1] = False
        
        # Extract graph from skeleton
        vertices, edges = fit_graph(skeleton)
        return vertices, edges
    
    # TODO helper to render graph
    
    # Compute line coordinates from graph
    def extract_lines(self, vertices, edges):
        vertices, edges = prune_graph(vertices, edges)
        neighbors = build_neighbors(vertices, edges)
        components = build_components(vertices, neighbors)
        lines = build_lines(vertices, neighbors, components)
        return lines
    
    # TODO helper to render lines
    
    # Generate annotations
    def build_annotation(self, lines, expected_thickness, ratio=1.0):
        rescaled_lines = []
        for points in lines:
            
            # Upscale
            points = numpy.array(points) * ratio
            
            # Create line
            # TODO somehow, at some point, estimate individual line thickness
            line = LineHelper.line_from_array(points, expected_thickness)
            
            # Adjust baseline
            line = LineHelper(line)
            line = LineHelper.line_from_array(line.points - 0.5 * line.boundaries, line.thickness)
            rescaled_lines.append(line)
        
        # Build annotation
        annotation = Annotation(lines=rescaled_lines, tags=['predicted'])
        return annotation
    
    # Apply all steps in a single call
    def tag(self, image, expected_thickness):
        network_image = self.prepare_image(image, expected_thickness)
        probability = self.compute_probability(network_image)
        vertices, edges = self.extract_graph(probability)
        lines = self.extract_lines(vertices, edges)
        ratio = (image.size[0] / network_image.size[0] + image.size[1] / network_image.size[1]) / 2
        annotation = self.build_annotation(lines, expected_thickness, ratio)
        return annotation
    
    # Draw line annotation on original image
    @classmethod
    def paint_annotation(cls, image, annotation):
        result = image.copy().convert('RGB')
        draw = ImageDraw.Draw(result, 'RGBA')
        for line in annotation.lines:
            l = LineHelper(line)
            polygon = numpy.concatenate([
                (l.points),
                (l.points + l.boundaries)[::-1]
            ], axis=0)
            draw.polygon(polygon, fill=(255, 0, 0, 128))
            for x, y in l.points:
                draw.ellipse([x - 3, y - 3, x + 3, y + 3], fill=(0, 0, 255, 128))
        return result
