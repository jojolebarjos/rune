# -*- coding: utf-8 -*-


import random
from PIL import Image

from rune.data.fake import enumerate_fonts, PatchGenerator, PatchAugmentor


# # Get available fonts
# paths = list(enumerate_fonts('font'))

# # Choose one
# path = random.choice(paths)

# # Create generator
# generator = PatchGenerator(path)
# augmentor = PatchAugmentor()

# # Generate some patch
# text = 'Hello, world!'
# patch, baseline = generator.compute_patch(text)
# patch = augmentor.resample(patch, baseline, generator.thickness)
# patch.save('foo.png')



from rune.data.fake import FakeSampler
import numpy
from PIL import Image
from tqdm import tqdm

fake = FakeSampler()

patches = []
for i in tqdm(range(8)):
    patch, text = fake.sample()
    patch = numpy.asarray(patch)
    patches.append(patch)

patch = numpy.concatenate(patches, axis=0)
patch = Image.fromarray(patch)
patch.save('foo6.png')

