# -*- coding: utf-8 -*-


import numpy
from PIL import Image

from .annotation import Area, Line, Point


# Line wrapper
class LineHelper:
    def __init__(self, line: Line, ratio: float = 1.0):
        self.line = line
        self.count = len(line.points)
        self.thickness = line.thickness * ratio
        
        # Get points as array
        self.points = numpy.zeros((self.count, 2), dtype=numpy.float32)
        for i in range(self.count):
            p = line.points[i]
            self.points[i, 0] = p.x * ratio
            self.points[i, 1] = p.y * ratio
        
        # Forward deltas
        self.forwards = self.points[1:] - self.points[:-1]
        
        # Compute lengths
        self.lengths = numpy.sqrt((self.forwards * self.forwards).sum(axis=1))
        self.length = self.lengths.sum()
        
        # Normalize forward deltas
        self.forwards /= self.lengths[:, numpy.newaxis]
        
        # Vertical unit vectors
        self.upwards = numpy.empty([self.count - 1, 2], dtype=numpy.float32)
        self.upwards[:, 0] = self.forwards[:, 1]
        self.upwards[:, 1] = -self.forwards[:, 0]
        
        # Unit normals
        self.normals = self.upwards[:-1] + self.upwards[1:]
        self.normals /= numpy.sqrt((self.normals * self.normals).sum(axis=1))[:, numpy.newaxis]
        self.normals = numpy.concatenate([self.upwards[:1], self.normals, self.upwards[-1:]], axis=0)
        
        # Thickness-aware normals (a.k.a. boundaries)
        # TODO check for per-point thickness
        self.boundaries = self.normals * self.thickness
    
    # Generate equidistant point layout
    def resample(self, distance=None, margin=0):
        
        # If distance is not specified, use thickness
        if distance is None:
            distance = self.thickness
        
        # Split into chunks, taking into account any margin
        count = int(numpy.round((self.length + margin * 2) / distance)) + 1
        
        # Small polylines keep only the extremities
        if count <= 2:
            resampled_points = self.points[[0, -1]]
        else:
        
            # Define linear interpolation
            def interpolate(t):
                for i, l in enumerate(self.lengths):
                    if t <= l:
                        break
                    t -= l
                else:
                    t += l
                a = self.points[i]
                b = self.points[i + 1]
                p = a + (b - a) * (t / l)
                return p
            
            # Generate points
            resampled_points = []
            for t in numpy.linspace(-margin, self.length + margin, count):
                p = interpolate(t)
                resampled_points.append(p)
        
        # Create a new line
        return LineHelper.line_from_array(resampled_points, self.thickness)
    
    # Add random noise to points (assumes uniformly sampled points)
    def randomize(self, low_std=0.2, high_std=0.1, translation_std=0.3):
        
        # Generate random piecewise linear offsets
        def peak():
            return numpy.clip(numpy.random.normal(0.0, low_std, 2), -2 * low_std, 2 * low_std)
        last_delta = peak()
        deltas = [last_delta]
        while len(deltas) < len(self.points):
            num = numpy.random.randint(4)
            next_delta = peak()
            for i in range(1, num + 1):
                delta = ((num - i) * last_delta + i * next_delta) / num
                deltas.append(delta)
            last_delta = next_delta
        deltas = numpy.array(deltas[:len(self.points)])
            
        # Apply smoothing
        for i in range(5):
            deltas[1:-1] = (deltas[:-2] + deltas[1:-1] + deltas[2:]) / 3
        
        # Add local noise
        deltas += numpy.random.normal(0.0, high_std, deltas.shape).clip(-2 * high_std, 2 * high_std)
        
        # Add global noise
        deltas += numpy.random.normal(0.0, translation_std, (1, 2)).clip(-2 * translation_std, 2 * translation_std)
        
        # Make sure left and right boundaries are not clipping things
        if deltas[0, 0] > 0:
            deltas[0, 0] = 0
        if deltas[-1, 0] < 0:
            deltas[-1, 0] = 0
        
        # Apply offsets to points
        points = self.points + deltas * self.thickness
        
        # Create a new line
        return LineHelper.line_from_array(points, self.thickness)
    
    # TODO random slant/italic
    
    # TODO random per-point thickness
    
    # Extract patch
    def extract(self, image, height=None, extent_ratio=1.0, interpolation=Image.BICUBIC):
        
        # If height is not provided, keep original scale
        if height is None:
            height = int(self.thickness * extent_ratio)
        
        # Define one block per segment
        ratio = height / (extent_ratio * self.thickness)
        widths = (self.lengths * ratio).astype(numpy.int32)
        width = widths.sum()
        
        # Generate blocks
        above_ratio = 0.5 * extent_ratio + 0.5
        below_ratio = -0.5 * extent_ratio + 0.5
        mesh = []
        left = 0
        for i in range(len(widths)):
            right = left + widths[i]
            box = (
                left, 0,
                right, height
            )
            quad = (
                self.points[i    , 0] + self.boundaries[i    , 0] * above_ratio, self.points[i    , 1] + self.boundaries[i    , 1] * above_ratio,
                self.points[i    , 0] + self.boundaries[i    , 0] * below_ratio, self.points[i    , 1] + self.boundaries[i    , 1] * below_ratio,
                self.points[i + 1, 0] + self.boundaries[i + 1, 0] * below_ratio, self.points[i + 1, 1] + self.boundaries[i + 1, 1] * below_ratio,
                self.points[i + 1, 0] + self.boundaries[i + 1, 0] * above_ratio, self.points[i + 1, 1] + self.boundaries[i + 1, 1] * above_ratio
            )
            part = (box, quad)
            mesh.append(part)
            left = right
        
        # Extract patch
        return image.transform((width, height), Image.MESH, mesh, interpolation)
    
    # Create new line from array
    @staticmethod
    def line_from_array(points, thickness):
        return Line(points=[Point(x=p[0], y=p[1]) for p in points], thickness=thickness)


# Area wrapper
class AreaHelper:
    def __init__(self, area: Area, ratio: float = 1.0):
        self.area = area
        self.count = len(area.points)
        
        # Get points as array
        self.points = numpy.zeros((self.count, 2), dtype=numpy.float32)
        for i in range(self.count):
            p = area.points[i]
            self.points[i, 0] = p.x * ratio
            self.points[i, 1] = p.y * ratio
