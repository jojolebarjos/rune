
# Rune: A Handwritten Character Recognition Playground

...


## Requirements

The following requirements are needed for the interface and the model:

```
pip install aiofiles fastapi freetype-py numpy Pillow scikit-image scipy skorch tqdm uvicorn
```

You also need PyTorch, see [their website](https://pytorch.org/get-started/locally/) for installation instructions.


## Annotation interface

Start the annotation web interface using:

```
uvicorn rune.service.server:app --host 127.0.0.1 --port 80
```

Documentation is then available at `http://127.0.0.1:80/docs`.
