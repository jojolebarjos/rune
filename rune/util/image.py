# -*- coding: utf-8 -*-


import numpy
from PIL import Image
import scipy.ndimage.filters
import skimage.morphology


# Convert to NumPy array
def as_array(image):
    array = numpy.asarray(image, dtype=numpy.float32)
    array /= 255.0
    return array


# Convert to Pillow image
def as_image(array):
    array = (array * 255).clip(0, 255).astype(numpy.uint8)
    image = Image.fromarray(array)
    return image


# Resize image by factor
def resize(image, ratio):
    width, height = image.size
    width = int(width * ratio)
    height = int(height * ratio)
    image = image.resize((width, height), Image.BICUBIC)
    return image


# Remove background and normalize brightness
def normalize(array, closing_radius=8, foreground_filter_size=64, min_foreground=0.4):
    
    # Estimate background intensity
    background = skimage.morphology.closing(array, skimage.morphology.disk(closing_radius))
    array = (background - array) / background.clip(min=1)
    
    # Estimate foreground intensity
    foreground = scipy.ndimage.filters.maximum_filter1d(array, foreground_filter_size, axis=0)
    foreground = scipy.ndimage.filters.maximum_filter1d(foreground, foreground_filter_size, axis=1)
    foreground = foreground.clip(min=min_foreground)
    array /= foreground
    
    return array
