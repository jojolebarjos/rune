# -*- coding: utf-8 -*-


import freetype
import numpy
import os


# Enumerate all font files (recursive)
def enumerate_fonts(folder):
    for name in os.listdir(folder):
        path = os.path.join(folder, name)
        if os.path.isdir(path):
            yield from enumerate_fonts(path)
        elif name.endswith('.ttf'):
            yield path


# Generate raw patches
class PatchGenerator:
    def __init__(self, path):
        
        # Load font
        self.path = path
        self.face = freetype.Face(path)
        
        # Set default size
        self.size = None
        self.set_size(64)
        
        # Estimate font thickness scale
        thicknesses = []
        for char in 'acegmnopqrsuvwxyz':
            self.face.load_char(char, freetype.FT_LOAD_RENDER)
            thickness = self.face.glyph.metrics.horiBearingY / 64
            thicknesses.append(thickness)
        self.thickness_scale = sum(thicknesses) / len(thicknesses) / self.size
    
    # Set new size, in pixels
    def set_size(self, size):
        self.size = size
        self.face.set_char_size(size * 64)
    
    @property
    def thickness(self):
        return self.thickness_scale * self.size
    
    # TODO maybe handle space separately, to randomize horizontal spacing between words?
    
    # Estimate text bounding box
    def compute_box(self, text):
        previous_char = 0
        pen_x, pen_y = 0, 0
        min_x, min_y = 0, 0
        max_x, max_y = 0, 0
        for char in text:
            self.face.load_char(char, freetype.FT_LOAD_RENDER)
            kerning = self.face.get_kerning(previous_char, char)
            previous_char = char
            
            # Get raw bitmap
            width  = self.face.glyph.bitmap.width
            rows   = self.face.glyph.bitmap.rows
            top    = self.face.glyph.bitmap_top
            left   = self.face.glyph.bitmap_left
            
            # Apply character bounding box
            pen_x += kerning.x
            x0 = (pen_x >> 6) + left
            x1 = x0 + width
            y0 = (pen_y >> 6) - (rows - top)
            y1 = y0 + rows
            
            # Update global bounding box
            min_x, max_x = min(min_x, x0), max(max_x, x1)
            min_y, max_y = min(min_y, y0), max(max_y, y1)
            
            # Move pointer
            pen_x += self.face.glyph.advance.x
            pen_y += self.face.glyph.advance.y
        
        return min_x, min_y, max_x, max_y
    
    # Accumulate glyphs in a single patch
    def compute_patch(self, text):
        
        # Compute bounding box
        box = self.compute_box(text)
        min_x, min_y, max_x, max_y = box
        
        # Iterate over characters
        buffer = numpy.zeros((max_y - min_y, max_x - min_x), dtype=numpy.uint8)
        previous_char = 0
        pen_x, pen_y = 0, 0
        for char in text:
            self.face.load_char(char, freetype.FT_LOAD_RENDER)
            kerning = self.face.get_kerning(previous_char, char)
            previous_char = char
            
            # Get raw bitmap
            bitmap = self.face.glyph.bitmap
            pitch  = self.face.glyph.bitmap.pitch
            width  = self.face.glyph.bitmap.width
            rows   = self.face.glyph.bitmap.rows
            top    = self.face.glyph.bitmap_top
            left   = self.face.glyph.bitmap_left
            
            # Place glyph
            pen_x += kerning.x
            x = (pen_x >> 6) - min_x + left
            y = (pen_y >> 6) - min_y - (rows - top)
            
            # Copy content
            for row in range(rows):
                data = bitmap.buffer[row * pitch : row * pitch + width]
                data = numpy.array(data, dtype=numpy.uint8)
                buffer[y + rows - row - 1, x : x + width] |= data
            
            # Move pointer
            pen_x += self.face.glyph.advance.x
            pen_y += self.face.glyph.advance.y
        
        # Flip vertically
        buffer = buffer[::-1]
        baseline = max_y
        return buffer, baseline
