# -*- coding: utf-8 -*-


import numpy


# Simplify and clean graph
def prune_graph(vertices, edges):
    
    # Get neighbors
    neighbors = build_neighbors(vertices, edges)
    
    # Resolve vertices with more than 2 edges
    edges = {(a, b) for a, b in edges}
    for i in range(len(vertices)):
        ns = neighbors[i]
        if len(ns) > 2:
            
            # Keep most horizontal edges
            ps = vertices[ns, :]
            ds = ps - vertices[i, None, :]
            ls = numpy.sqrt((ds * ds).sum(axis=1))
            ds /= ls[:, None]
            scores = numpy.abs(ds[:, 0])
            for j in numpy.argsort(scores)[2:]:
                e = (i, ns[j])
                if e in edges:
                    edges.remove(e)
                e = (ns[j], i)
                if e in edges:
                    edges.remove(e)
    edges = numpy.array(list(edges))
    
    # TODO need to handle lines that are incorrectly connected at their extremity (i.e. C shape structure)
    
    # TODO maybe try to connect almost colinear segments with small gap
    
    # Get components
    neighbors = build_neighbors(vertices, edges)
    components = build_components(vertices, neighbors)
    
    # Keep large components only
    indices = set()
    min_length = 32
    for component in components:
        length = 0
        for a in component:
            for b in neighbors[a]:
                if a < b:
                    d = vertices[a] - vertices[b]
                    l = numpy.sqrt((d * d).sum())
                    length += l
        if length >= min_length:
            indices.update(component)
    
    # Prune graph
    new_to_old = list(indices)
    old_to_new = {old : new for new, old in enumerate(new_to_old)}
    es = []
    for a, b in edges:
        if a in indices and b in indices:
            es.append((old_to_new[a], old_to_new[b]))
    vs = vertices[new_to_old]
    
    # Return new graph
    vertices = numpy.asarray(vs)
    edges = numpy.asarray(es)
    return vertices, edges
    

# Build neighbor list
def build_neighbors(vertices, edges):
    neighbors = [[] for _ in vertices]
    for a, b in edges:
        neighbors[a].append(b)
        neighbors[b].append(a)
    return neighbors


# Build connected components
def build_components(vertices, neighbors):
    components = []
    remaining = set(range(len(vertices)))
    while len(remaining) > 0:
        i = remaining.pop()
        component = {i}
        def recurse(i):
            for j in neighbors[i]:
                if not j in component:
                    component.add(j)
                    remaining.remove(j)
                    recurse(j)
        recurse(i)
        components.append(component)
    return components


# Build polylines
def build_lines(vertices, neighbors, components):
    lines = []
    for component in components:
        
        # Ignore invalid components
        invalid = False
        if len(component) < 2:
            invalid = True
        else:
            for i in component:
                if len(neighbors[i]) > 2:
                    invalid = True
                    break
        if invalid:
            continue
        
        # Find leftmost end
        ends = [i for i in component if len(neighbors[i]) == 1]
        assert len(ends) == 2
        ends.sort(key=lambda i: vertices[i, 1])
        
        # Follow until the end
        last_index = None
        index = ends[0]
        points = []
        while True:
            vertex = vertices[index]
            point = (vertex[1], vertex[0])
            points.append(point)
            next_indices = [i for i in neighbors[index] if i != last_index]
            if len(next_indices) == 0:
                break
            last_index = index
            index = next_indices[0]
        
        # Create line
        # TODO expected ratio is 10, as it is standard for network... but need to scale up thickness and points using original ratio
        lines.append(points)
    
    return lines
