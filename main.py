# -*- coding: utf-8 -*-


import json
import os
from PIL import Image
from tqdm import tqdm

import torch

from rune.data import Configuration, Sample, Annotation, Workspace
from rune.model.detector import DetectorCache, MultiSampler, CachedSampler, DetectorTrainer, DetectorTagger


# Create default configuration
configuration = Configuration()
configuration.device = 'cuda'


# Use local workspace
workspace = Workspace('workspace')

# Define cache
cache = DetectorCache('model/cache', configuration)

# If cache is empty, load from workspace
if len(cache) == 0:
    
    # Get available samples
    items = []
    for collection in workspace.values():
        for sample in collection.values():
            if 'user' in sample:
                annotation = sample['user']
                if 'complete' in annotation.tags:
                    item = (sample, annotation)
                    items.append(item)
    
    # Process samples
    for sample, annotation in tqdm(items):
        cache.add(sample.image_path, annotation)
    

# Create sampler cache, if needed
sampler_path = 'model/sampler.pkl'
if not os.path.exists(sampler_path):
    base_sampler = MultiSampler.from_cache(cache)
    oversampled_base_sampler = (pair for _ in range(3) for pair in base_sampler)
    CachedSampler.create(sampler_path, tqdm(oversampled_base_sampler))

# Load sampler
def loop(sampler):
    while True:
        for sample in sampler:
            yield sample
sampler = loop(CachedSampler(sampler_path))


# Create trainer
trainer = DetectorTrainer(configuration, sampler)

# Restore model, if any
if os.path.exists('model/checkpoint.pt'):
    trainer.model.load_state_dict(torch.load('model/checkpoint.pt'))

# Create tagger for periodic test
tagger = DetectorTagger(configuration, trainer.model)

# Load test image
test_image = Image.open('workspace/my_collection/IMG_20190626_172042_594.jpg')
test_image = tagger.prepare_image(test_image, 30)

# Rune several steps
num_epochs = 20
num_steps = 100
for epoch in range(num_epochs):
    
    # Train several steps
    trainer.model.train()
    mean_loss = None
    with tqdm(range(num_steps)) as progress:
        for step in progress:
            batch_loss = trainer.do_step()
            if mean_loss is None:
                mean_loss = batch_loss
            else:
                mean_loss = mean_loss * 0.9 + batch_loss * 0.1
            progress.set_description(f'{mean_loss:.4f} ({batch_loss:.4f})')
    
    # Evaluate on test image
    trainer.model.eval()
    probability = tagger.compute_probability(test_image)
    probability = tagger.color_probability(probability)
    probability.save(f'model/test.{epoch:03d}.png')
    torch.save(trainer.model.state_dict(), f'model/checkpoint.{epoch:03d}.pt')

# Save model
torch.save(trainer.model.state_dict(), 'model/checkpoint.pt')


# Create tagger
tagger = DetectorTagger(configuration, 'model/checkpoint.pt')

# Load test image
image = Image.open('workspace/my_collection/IMG_20190626_171735_551.jpg')

# Apply model
prepared_image = tagger.prepare_image(image, 30)
probability = tagger.compute_probability(prepared_image)
debug_image = tagger.color_probability(probability)
debug_image.save('debug.png')
